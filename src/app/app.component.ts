import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
  	.textColor {
  		color: white;
  	}
  `]
})
export class AppComponent {

  private show: boolean = false;
  private events: Array < number > = [];

  toggle() {
    this.events.push(+new Date);
    this.show = !this.show;
  }
  getShow() {
    return this.show;
  }

  getBackgroundColor(count: number) {
    if (count >= 4) {
      return 'blue';
    }
  }

  getTextColor(count: number) {
    if (count >= 5) {
      return true;
    } 
  }

  getEvents() {
    return this.events;
  }
}
